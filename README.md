# Projet Dilemme du Prisonnier

Le projet "Dilemme du Prisonnier" est une implémentation en Java du célèbre jeu "Dilemme du Prisonnier". Dans ce jeu, deux joueurs font face à des décisions de coopération ou de trahison, et les récompenses ou les sanctions dépendent des choix de chaque joueur. Le but de ce projet est de créer diverses stratégies de joueurs et de les faire concourir pour analyser leurs performances dans le contexte du dilemme.

## Classes et Interfaces

Ce projet comporte plusieurs classes et une interface pour représenter différents types de joueurs dans le jeu. Chaque classe ou interface a une stratégie de jeu unique.

### Player

La classe abstraite `Player` est utilisée comme base pour les classes de joueurs dans le jeu "Dilemme du Prisonnier". Elle définit les méthodes requises pour obtenir le nom du joueur et prendre une décision de jeu en fonction du nom de l'adversaire.

- `public abstract String getName()`: Cette méthode est abstraite et doit être implémentée par les classes qui utilisent l'interface Player. Elle est responsable de renvoyer le nom du joueur, qui sera utilisé pour identifier le joueur dans le contexte du jeu.

- `public abstract int play(String opponentName)`: Cette méthode est abstraite et doit également être implémentée par les classes qui utilisent l'interface Player. Elle est responsable de prendre une décision de jeu en fonction du nom de l'adversaire.

### Collaborator

La classe `Collaborator` représente un type de joueur dans le jeu "Dilemme du Prisonnier" qui coopère systématiquement.

- `public String getName()`: Cette méthode retourne le nom du joueur "Collaborator", qui est "collaborator". Cela permet d'identifier le joueur dans le contexte du jeu.

- `public int play(String opponentName)`: Cette méthode est responsable de la prise de décision du joueur "Collaborator" lorsqu'il est confronté à un adversaire. Peu importe l'opposant, le "Collaborator" choisira toujours de coopérer en retournant la valeur de la constante "Game.COLLABORATE".

### ComplexPlayer

L'interface `ComplexPlayer` est conçue pour les joueurs ayant des stratégies de jeu plus complexes. Elle définit deux méthodes importantes pour ces joueurs.

- `void updateLastOpponentMove(int OpponentMove)`: Cette méthode est appelée pour mettre à jour la dernière action effectuée par l'adversaire. Le paramètre "OpponentMove" représente l'action de l'adversaire, généralement sous forme de valeur numérique ou d'autres données appropriées. Les classes qui implémentent cette interface doivent définir comment elles utilisent cette information pour prendre des décisions de jeu.

- `void reset()`: La méthode "reset" est appelée pour réinitialiser l'état du joueur après chaque manche du jeu. Cela peut inclure la réinitialisation des données ou des variables internes qui sont utilisées pour prendre des décisions. Les classes qui implémentent cette interface définiront la logique spécifique de réinitialisation nécessaire pour leur stratégie.

### Denouncer

La classe `Denouncer` représente un joueur qui dénonce systématiquement, peu importe l'opposant.

- `public String getName()`: Cette méthode retourne le nom du joueur "Denouncer", qui est "denouncer". Cela permet d'identifier le joueur dans le contexte du jeu.

- `public int play(String opponentName)`: Cette méthode est responsable de la prise de décision du joueur "Denouncer" lorsqu'il est confronté à un adversaire. Quel que soit l'opposant, le "Denouncer" choisira toujours de dénoncer en retournant la valeur de la constante "Game.DENOUNCE".

### DonnantDonnant

La classe `DonnantDonnant` représente un joueur qui commence par coopérer et suit la stratégie "Donnant Donnant".

- `public String getName()`: Cette méthode retourne le nom du joueur "DonnantDonnant", qui est "DonnantDonnant". Cela permet d'identifier le joueur dans le contexte du jeu.

- `public int play(String opponentName)`: Cette méthode est responsable de la prise de décision du joueur "DonnantDonnant" lorsqu'il est confronté à un adversaire. Le joueur "DonnantDonnant" retourne le dernier coup de l'adversaire, stocké dans la variable "previousMove".

- `public void updateLastOpponentMove(int opponentMove)`: Cette méthode permet de mettre à jour le dernier coup de l'adversaire en fonction de l'action de l'adversaire. Le joueur "DonnantDonnant" utilise cette information pour déterminer sa prochaine décision.

### Graduelle

La classe `Graduelle` représente un joueur qui commence par coopérer et peut passer en mode de rétorsion.

- `public String getName()`: Cette méthode retourne le nom du joueur "Graduelle", qui est "Graduelle". Cela permet d'identifier le joueur dans le contexte du jeu.

- `public int play(String opponentName)`: Cette méthode est responsable de la prise de décision du joueur "Graduelle" lorsqu'il est confronté à un adversaire. La stratégie de jeu dépend de la variable "retaliating" et du nombre de trahisons consécutives "consecutiveBetrayals".

- `public void updateLastOpponentMove(int opponentMove)`: Cette méthode permet de mettre à jour le nombre de trahisons consécutives en fonction de l'action de l'adversaire. Si l'adversaire trahit, le joueur "Graduelle" peut passer en mode de rétorsion en augmentant le nombre de trahisons consécutives.

### Mefiant

La classe `Mefiant` représente un joueur qui trahit systématiquement, quel que soit l'opposant.

- `public String getName()`: Cette méthode retourne le nom du joueur "Mefiant", qui est "Mefiant". Cela permet d'identifier le joueur dans le contexte du jeu.

- `public int play(String opponentName)`: Cette méthode est responsable de la prise de décision du joueur "Mefiant" lorsqu'il est confronté à un adversaire. Quel que soit l'opposant, le "Mefiant" choisira toujours de trahir en retournant la valeur de la constante "Game.DENOUNCE".

- `public void updateLastOpponentMove(int opponentMove)`: Cette méthode est définie dans le cadre de l'interface "ComplexPlayer", mais dans le cas du "Mefiant", elle ne met pas à jour la stratégie du joueur, car ce dernier maintient toujours la même décision.

### Random

La classe `Random` représente un joueur qui prend des décisions de manière aléatoire, sans considération de l'adversaire ou des décisions précédentes.

- `public String getName()`: Cette méthode retourne le nom du joueur "Random", qui est "random". Cela permet d'identifier le joueur dans le contexte du jeu.

- `public int play(String opponentName)`: Cette méthode est responsable de la prise de décision du joueur "Random" lorsqu'il est confronté à un adversaire. Le joueur "Random" prend une décision aléatoire en utilisant la fonction Math.random().

### Sondeur

La classe `Sondeur` représente un joueur qui suit une stratégie complexe basée sur l'observation des coups précédents de l'adversaire.

- `public String getName()`: Cette méthode retourne le nom du joueur "Sondeur", qui est "Sondeur". Cela permet d'identifier le joueur dans le contexte du jeu.

- `public int play(String opponentName)`: Cette méthode est responsable de la prise de décision du joueur "Sondeur" lorsqu'il est confronté à un adversaire. La stratégie de jeu dépend du tour actuel, de l'observation des coups précédents de l'adversaire et du nombre de coups déjà joués.

- `public void updateLastOpponentMove(int opponentMove)`: Cette méthode permet de mettre à jour les trois derniers coups de l'adversaire dans le tableau "opponentMoves". Cela permet au joueur "Sondeur" d'observer les actions de l'adversaire pour prendre des décisions informées.

Chaque classe ou interface a une description complète de ses méthodes, attributs et utilisation dans le contexte du jeu "Dilemme du Prisonnier".
