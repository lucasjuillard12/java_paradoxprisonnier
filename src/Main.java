import prisoners.Arena;
import prisoners.players.*;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Player p1 = new Collaborator();
        Player p2 = new Denouncer();
        Player p3 = new Random();
        Player p4 = new DonnantDonnant();
        Player p5 = new Graduelle();
        Player p6 = new PeriodiqueTtc();
        Player p7 = new Rancunier();
        Player p8 = new Sondeur();
        Player p9 = new Mefiant();

        ArrayList<Player> competiteur = new ArrayList<Player>();
        competiteur.add(p1);
        competiteur.add(p2);
        competiteur.add(p3);
        competiteur.add(p4);
        competiteur.add(p5);
        competiteur.add(p6);
        competiteur.add(p7);
        competiteur.add(p8);
        competiteur.add(p9);


        Arena arena = new Arena(competiteur);

        arena.playRounds(10);

        System.out.println(arena.getScoretoString());

        arena.printRanking();
    }
}