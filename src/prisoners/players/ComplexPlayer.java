package prisoners.players;

public interface ComplexPlayer {
    void updateLastOpponentMove(int OpponnentMove);
    void reset();
}
