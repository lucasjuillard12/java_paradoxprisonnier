package prisoners.players;
import prisoners.Game;
public class Rancunier extends Player implements ComplexPlayer {
    private boolean cooperate = true; // On commence par coopérer

    @Override
    public String getName() {
        return "Rancunier";
    }

    @Override
    public int play(String opponentName) {
        if (cooperate) {
            return Game.COLLABORATE;
        } else {
            return Game.DENOUNCE;
        }
    }

    @Override
    public void updateLastOpponentMove(int opponentMove) {
        if (opponentMove == Game.DENOUNCE) {
            cooperate = false; // Dès que l'adversaire trahit, on trahit indéfiniment
        }
    }

    @Override
    public void reset() {
        cooperate = true;
    }
}
