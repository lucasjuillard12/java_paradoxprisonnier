package prisoners.players;
import prisoners.Game;
public class MajoriteMou extends Player implements ComplexPlayer {
    private int consecutiveBetrayals = 0; // Nombre de trahisons consécutives de l'adversaire
    private boolean cooperate = true; // On commence par coopérer

    @Override
    public String getName() {
        return "MajoriteMou";
    }

    @Override
    public int play(String opponentName) {
        if (cooperate) {
            return Game.COLLABORATE;
        } else {
            return Game.DENOUNCE;
        }
    }

    @Override
    public void updateLastOpponentMove(int opponentMove) {
        if (opponentMove == Game.DENOUNCE) {
            consecutiveBetrayals++;
        } else {
            if (consecutiveBetrayals >= 1) {
                cooperate = false; // Si l'adversaire a trahi au moins une fois consécutivement, on trahit
            }
            // vérifier si c'est le comportement qu'on souhaite ???
        }
    }

    @Override
    public void reset() {
        consecutiveBetrayals = 0;
        cooperate = true;
    }
}
