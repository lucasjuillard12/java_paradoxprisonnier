package prisoners.players;
import prisoners.Game;
public class DonnantDonnant extends Player implements ComplexPlayer {
    private int previousMove = Game.COLLABORATE; // On commence par coopérer

    @Override
    public String getName() {
        return "DonnantDonnant";
    }

    @Override
    public int play(String opponentName) {
        // Retourne le coup précédent de l'adversaire
        return previousMove;
    }

    // Met à jour le coup précédent de l'adversaire
    public void updateLastOpponentMove(int opponentMove) {
        previousMove = opponentMove;
    }

    @Override
    public void reset() {
        previousMove = Game.COLLABORATE;
    }
}
