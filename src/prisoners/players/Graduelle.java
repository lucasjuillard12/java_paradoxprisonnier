package prisoners.players;
import prisoners.Game;
public class Graduelle extends Player implements ComplexPlayer {
    private int consecutiveBetrayals; // Nombre de trahisons consécutives de l'adversaire
    private boolean retaliating; // Indique si l'on est en mode de rétorsion

    public Graduelle() {
        consecutiveBetrayals = 0;
        retaliating = false;
    }

    @Override
    public String getName() {
        return "Graduelle";
    }

    @Override
    public int play(String opponentName) {
        if (retaliating) {
            // En mode rétorsion, on trahit
            consecutiveBetrayals--;
            if (consecutiveBetrayals == 0) {
                retaliating = false; // Terminé avec la rétorsion, on coopère à nouveau
            }
            return Game.DENOUNCE;
        } else {
            // En mode coopération, on coopère sauf si l'adversaire trahit
            if (consecutiveBetrayals > 0) {
                // Après une période de rétorsion, on coopère deux fois
                consecutiveBetrayals--;
                if (consecutiveBetrayals == 0) {
                    return Game.COLLABORATE;
                }
                return Game.COLLABORATE;
            } else {
                // On coopère sauf si l'adversaire trahit
                return Game.COLLABORATE;
            }
        }
    }

    @Override
    public void updateLastOpponentMove(int opponentMove) {
        if (opponentMove == Game.DENOUNCE) {
            consecutiveBetrayals = consecutiveBetrayals + 1;
            retaliating = true;
        } else if (consecutiveBetrayals > 0) {
            consecutiveBetrayals = consecutiveBetrayals - 1;
        }
    }

    @Override
    public void reset() {
        consecutiveBetrayals = 0;
        retaliating = false;
    }
}
