package prisoners.players;
import prisoners.Game;

public class Mefiant extends Player implements ComplexPlayer{

    @Override
    public String getName() {
        return "Mefiant";
    }

    private int previousMove = Game.DENOUNCE; // On commence par trahir

    @Override
    public int play(String opponentName) {
        // Retourne le coup précédent de l'adversaire
        return previousMove;
    }

    // Met à jour le coup précédent de l'adversaire
    public void updateLastOpponentMove(int opponentMove) {
        previousMove = opponentMove;
    }

    @Override
    public void reset() {
        previousMove = Game.DENOUNCE;
    }
}
