package prisoners.players;
import prisoners.Game;
public class PeriodiqueTtc extends Player  {
    private int[] moves = {Game.DENOUNCE, Game.DENOUNCE, Game.COLLABORATE}; // Tableau des coups à jouer
    private int currentIndex = 0; // Indice du prochain coup à jouer

    @Override
    public String getName() {
        return "PeriodiqueTtc";
    }

    @Override
    public int play(String opponentName) {
        int move = moves[currentIndex];
        currentIndex = (currentIndex + 1) % moves.length; // Passer au prochain coup
        return move;
    }

}
