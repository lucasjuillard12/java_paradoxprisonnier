package prisoners.players;
import prisoners.Game;
public class Sondeur extends Player implements ComplexPlayer {
    private int round;
    private int[] opponentMoves;

    public Sondeur() {
        round = 0;
        opponentMoves = new int[3]; // Stocke les 3 derniers coups de l'adversaire
    }

    @Override
    public String getName() {
        return "Sondeur";
    }

    @Override
    public int play(String opponentName) {
        round++;

        if (round <= 3) {
            // Trahir-coopérer-coopérer pour les trois premiers coups
            if (round == 1) {
                return Game.DENOUNCE; // Trahir au premier coup
            } else {
                return Game.COLLABORATE; // Coopérer aux coups 2 et 3
            }
        } else {
            if (round == 4 && opponentMoves[1] == Game.COLLABORATE && opponentMoves[2] == Game.COLLABORATE) {
                // Trahir au quatrième round si l'adversaire a coopéré aux coups 2 et 3
                return Game.DENOUNCE;
            } else {
                // Coopérer puis jouer le coup précédent de l'adversaire
                return opponentMoves[2];
            }
        }
    }

    @Override
    public void updateLastOpponentMove(int opponentMove) {
        // Mettre à jour les trois derniers coups de l'adversaire
        opponentMoves[0] = opponentMoves[1];
        opponentMoves[1] = opponentMoves[2];
        opponentMoves[2] = opponentMove;
    }

    @Override
    public void reset() {
        round = 0;
        opponentMoves = new int[3];
    }
}

