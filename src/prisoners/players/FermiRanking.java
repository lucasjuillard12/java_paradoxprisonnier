package prisoners.players;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;

public class FermiRanking {
    // on a malheuresement pas eu le temps de l'utiliser dans Arena mais voici l'implémentation du Ranking de Fermi
    private List<Player> players;
    private Hashtable<Player, Integer> ranking;

    public FermiRanking(List<Player> players) {
        this.players = players;
        this.ranking = new Hashtable<>();
        initializeRanking();
    }

    private void initializeRanking() {
        for (Player player : players) {
            ranking.put(player, 0);
        }
    }

    public void updateRanking(Player winner, Player loser) {
        int wins = ranking.get(winner);
        ranking.put(winner, wins + 1);

        int losses = ranking.get(loser);
        ranking.put(loser, losses + 1);
    }

    public void printRanking() {
        List<Player> sortedPlayers = new ArrayList<>(players);
        sortedPlayers.sort(Comparator.comparingInt(ranking::get));

        int rank = 1;
        for (Player player : sortedPlayers) {
            System.out.println("Position " + rank + ": " + player.getName() + " - Victoires : " + ranking.get(player));
            rank++;
        }
    }
}
