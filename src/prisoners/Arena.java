package prisoners;

import prisoners.players.ComplexPlayer;
import prisoners.players.Player;

import java.util.*;

public class Arena {

    private final ArrayList<Player> players;
    private final Hashtable<Player, Double> totalScore;
    private final Hashtable<Player, Integer> nbVictoire;

    public Arena(ArrayList<Player> players){
        this.players = new ArrayList<>(players);
        this.totalScore = new Hashtable<>();
        this.nbVictoire = new Hashtable<>();
        for (Player player: this.players) {
            assert player != null : "You can't have a null value in ArrayList<Player> players";
            this.totalScore.put(player, 0.0);
            this.nbVictoire.put(player, 0);
        }
    }



    public void playRounds(int nbRound){
        ArrayList<Player> tempPlayer = new ArrayList<>(this.players);
        for (Player player1 : this.players) {
            tempPlayer.remove(player1);
            for (Player player2 : tempPlayer){
                double scoreP1 = 0;
                double scoreP2 = 0;
                for(int i = 0; i < nbRound ; i++ ) {
                    double[] result = Game.play(player1, player2);
                    scoreP1 += result[0];
                    scoreP2 += result[1];

                    if (player1 instanceof ComplexPlayer) this.updateLastOpponentMove((ComplexPlayer) player1,result[0]);
                    if (player2 instanceof ComplexPlayer) this.updateLastOpponentMove((ComplexPlayer) player2, result[1]);
                }
                if (scoreP1 > scoreP2) this.nbVictoire.put(player1, this.nbVictoire.get(player1) + 1);
                if (scoreP1 < scoreP2) this.nbVictoire.put(player2, this.nbVictoire.get(player2) + 1);
                if (player1 instanceof ComplexPlayer) ((ComplexPlayer) player1).reset();
                if (player2 instanceof ComplexPlayer) ((ComplexPlayer) player2).reset();
                this.totalScore.put(player1, this.totalScore.get(player1) + scoreP1);
                this.totalScore.put(player2, this.totalScore.get(player2) + scoreP2);
            }
        }
    }

    public void updateLastOpponentMove(ComplexPlayer player, double result){
        if (result == Game.BETRAY | result ==Game.DENY ) player.updateLastOpponentMove(Game.COLLABORATE);
        if (result ==Game.GOT_BETRAYED | result ==Game.MUTUAL_BETRAY ) player.updateLastOpponentMove(Game.DENOUNCE);
    }

    public String getScoretoString() {
        StringBuilder outputString = new StringBuilder();
       for(Player player : this.players){
           outputString.append(player.getName() + " : " + this.totalScore.get(player).toString() + "\n");
       }
       return outputString.toString();
    }


    public void printRanking() {
        // Crée une liste de joueurs triés en fonction de leur score
        List<Player> sortedPlayers = new ArrayList<>(players);
        Collections.sort(sortedPlayers, new PlayerComparator());

        System.out.println("Classement des joueurs :");
        int rank = 1;
        for (Player player : sortedPlayers) {
            double score = totalScore.get(player);
            int victories = nbVictoire.get(player);
            System.out.println("Position " + rank + ": " + player.getName() + " - Score : " + score + " - Victoires : " + victories);
            rank++;
        }
    }

    // Comparateur de joueurs pour le classement
    private class PlayerComparator implements Comparator<Player> {
        @Override
        public int compare(Player player1, Player player2) {
            double score1 = totalScore.get(player1);
            double score2 = totalScore.get(player2);

            // Trie en ordre décroissant de score
            if (score1 > score2) {
                return -1;
            } else if (score1 < score2) {
                return 1;
            } else {
                return 0;
            }
        }
    }
}
